package br.com.senac.ex2;

public class CalcularPrecoCarro {

    public double Calcular(Carro carro) {
        PrecoFinalCarro precoFinalCarro = new PrecoFinalCarro(0.28, 0.45);
        return precoFinalCarro.getValorFinal(carro);
    }

}
