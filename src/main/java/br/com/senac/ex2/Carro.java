package br.com.senac.ex2;

public class Carro {

    private String marca;
    private double custoFabrica;

    public Carro() {
    }

    public Carro(String marca, double custoFabrica) {
        this.marca = marca;
        this.custoFabrica = custoFabrica;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public double getCustoFabrica() {
        return custoFabrica;
    }

    public void setCustoFabrica(double custoFabrica) {
        this.custoFabrica = custoFabrica;
    }

}
