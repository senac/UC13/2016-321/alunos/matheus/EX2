package br.com.senac.ex2;

public class PrecoFinalCarro {

    private Double porcentagemDistribuidor;
    private Double impostos;

    public PrecoFinalCarro() {
    }

    public PrecoFinalCarro(Double porcentagemDistribuidor, Double impostos) {
        this.porcentagemDistribuidor = porcentagemDistribuidor;
        this.impostos = impostos;
    }

    public Double getPorcentagemDistribuidor() {
        return porcentagemDistribuidor;
    }

    public void setPorcentagemDistribuidor(Double porcentagemDistribuidor) {
        this.porcentagemDistribuidor = porcentagemDistribuidor;
    }

    public Double getImpostos() {
        return impostos;
    }

    public void setImpostos(Double impostos) {
        this.impostos = impostos;
    }

    public double getValorFinal(Carro carro) {
        return carro.getCustoFabrica() + carro.getCustoFabrica() * this.porcentagemDistribuidor + carro.getCustoFabrica() * this.impostos;
    }

}
