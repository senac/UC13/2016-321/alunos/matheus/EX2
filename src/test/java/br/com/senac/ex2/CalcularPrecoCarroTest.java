package br.com.senac.ex2;

import org.junit.Assert;
import org.junit.Test;

public class CalcularPrecoCarroTest {

    @Test
    public void deveCalcularOPrecoFinalDoCarro() {

        CalcularPrecoCarro calcularPrecoCarro = new CalcularPrecoCarro();
        Carro carro = new Carro("Ford", 10000);
        double valorFinal = calcularPrecoCarro.Calcular(carro);
        Assert.assertEquals(10000 + 10000 * 0.28 + 10000 * 0.45, valorFinal, 0.01);

    }

}
